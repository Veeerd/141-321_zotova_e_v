# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('organizators', '0005_auto_20150530_2120'),
    ]

    operations = [
        migrations.AlterField(
            model_name='organizators',
            name='description',
            field=models.TextField(),
        ),
    ]
