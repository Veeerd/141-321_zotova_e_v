# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('organizators', '0004_auto_20150530_1906'),
    ]

    operations = [
        migrations.AlterField(
            model_name='organizators',
            name='userphoto',
            field=models.ImageField(upload_to=b'users/photo', blank=True),
        ),
    ]
