# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('organizators', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Organizators',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('fio', models.CharField(max_length=70)),
                ('description', models.CharField(max_length=255)),
                ('photo_link', models.CharField(max_length=600)),
            ],
        ),
        migrations.DeleteModel(
            name='Organ',
        ),
    ]
