# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('organizators', '0002_auto_20150530_1804'),
    ]

    operations = [
        migrations.AlterField(
            model_name='organizators',
            name='photo_link',
            field=models.CharField(max_length=100),
        ),
    ]
