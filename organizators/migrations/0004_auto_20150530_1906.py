# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('organizators', '0003_auto_20150530_1822'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='organizators',
            name='photo_link',
        ),
        migrations.AddField(
            model_name='organizators',
            name='userphoto',
            field=models.ImageField(default=0, upload_to=b'users/photo'),
            preserve_default=False,
        ),
    ]
