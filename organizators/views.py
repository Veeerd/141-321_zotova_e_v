#! coding: utf-8
from django.shortcuts import render

# функция генерирующая 404 страницу
from django.http import Http404

# Create your views here.
from .models import Organizators
def index(request):
	organizators_list = Organizators.objects.all()
	context = {'organizators_list': organizators_list}
	return render(request, 'organizators/index.html', context)
	
def detail(request, organizator_id):
	try:
		organizator= Organizators.objects.get(id=organizator_id)
	except Organizators.DoesNotExist:
		#если такого поста нет, то генерируем 404
		#raise Http404
		#включенный DEBUG=True блокирует отображение
		#стандартных шаблонов, по этому использую прямое включение
		return render(request, '404.html')
	return render(request, 'organizators/detail.html', {'organizator': organizator})