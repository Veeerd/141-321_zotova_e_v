from django.conf.urls import patterns, url

from organizators import views
urlpatterns = patterns('',
	url(r'^$', views.index, name='index'),
    url(r'^(?P<organizator_id>[0-9]+)/$', views.detail, name='detail'),
)