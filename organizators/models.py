from django.db import models

class Organizators(models.Model):
	fio = models.CharField(max_length=70)
	description = models.TextField()
	userphoto = models.ImageField(upload_to='users/photo',blank=True)
 	def __str__(self):
		return self.fio