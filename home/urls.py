from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings

from django.shortcuts import render
from django.conf.urls import handler404
from django.conf.urls import handler500

def error404(request):
	return render(request, '404.html', status=404)
	
def error500(request):
	return render(request, '500.html', status=500)
	
handler404 = error404
handler500 = error500

urlpatterns = patterns('',
	url(r'^$', include('main.urls', namespace="main")),
	url(r'^news/', include('news.urls', namespace="news")),
	url(r'^organizators/', include('organizators.urls', namespace="organizators")),
	url(r'^contact/', include('contact.urls', namespace="contact")),
	url(r'^partners/', include('partners.urls', namespace="partners")),
	url(r'^presentation/', include('presentation.urls', namespace="presentation")),
	url(r'^spikers/', include('spikers.urls', namespace="spikers")),
    url(r'^admin/', include(admin.site.urls)),
	url(r'^pages/', include('django.contrib.flatpages.urls')),
)

if settings.DEBUG:
    urlpatterns += patterns('',
         (r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
    )

