#! coding: utf-8
from django.shortcuts import get_object_or_404, render

#Кэширование на уровне представлений http://djbook.ru/rel1.5/topics/cache.html#the-per-view-cache
from django.views.decorators.cache import cache_page

# Create your views here.
from .models import FlatPage

@cache_page(60 * 5)
def index(request):
	output = get_object_or_404(FlatPage, id=1)
	return render(request, 'contact/index.html', {'output': output})