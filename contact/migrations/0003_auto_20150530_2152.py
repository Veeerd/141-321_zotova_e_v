# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('contact', '0002_auto_20150530_2150'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='flatpage',
            name='url',
        ),
        migrations.AddField(
            model_name='flatpage',
            name='content',
            field=models.TextField(default=0),
            preserve_default=False,
        ),
    ]
