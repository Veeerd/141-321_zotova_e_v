from django.conf.urls import patterns, url
from . import views

urlpatterns = patterns('',
	url(r'^$', views.index, name='index'),
	url(r'^(?P<user_id>[0-9]+)/$', views.detail, name='detail'),
)