# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('spikers', '0003_spikers_presentation'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='spikers',
            name='presentation',
        ),
    ]
