# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('spikers', '0007_auto_20150530_1906'),
    ]

    operations = [
        migrations.AlterField(
            model_name='spikers',
            name='userphoto',
            field=models.ImageField(null=True, upload_to=b'users/photo'),
        ),
    ]
