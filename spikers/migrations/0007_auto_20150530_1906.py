# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('spikers', '0006_auto_20150530_1822'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='spikers',
            name='photo_link',
        ),
        migrations.AddField(
            model_name='spikers',
            name='userphoto',
            field=models.ImageField(default=0, upload_to=b'users/photo'),
            preserve_default=False,
        ),
    ]
