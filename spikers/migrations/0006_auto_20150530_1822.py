# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('spikers', '0005_spikers_photo_link'),
    ]

    operations = [
        migrations.AlterField(
            model_name='spikers',
            name='photo_link',
            field=models.CharField(max_length=100),
        ),
    ]
