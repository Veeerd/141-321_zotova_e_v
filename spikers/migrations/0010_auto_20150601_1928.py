# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('spikers', '0009_auto_20150530_2120'),
    ]

    operations = [
        migrations.AlterField(
            model_name='spikers',
            name='description',
            field=models.TextField(),
        ),
    ]
