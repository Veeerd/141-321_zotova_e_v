# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('spikers', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='spikers',
            old_name='title',
            new_name='fio',
        ),
        migrations.RemoveField(
            model_name='spikers',
            name='pub_date',
        ),
        migrations.RemoveField(
            model_name='spikers',
            name='text',
        ),
    ]
