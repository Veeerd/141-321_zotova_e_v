# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('spikers', '0004_remove_spikers_presentation'),
    ]

    operations = [
        migrations.AddField(
            model_name='spikers',
            name='photo_link',
            field=models.CharField(default=0, max_length=600),
            preserve_default=False,
        ),
    ]
