# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('presentation', '0006_remove_presentation_user_id'),
        ('spikers', '0002_auto_20150525_2055'),
    ]

    operations = [
        migrations.AddField(
            model_name='spikers',
            name='presentation',
            field=models.ManyToManyField(to='presentation.Presentation'),
        ),
    ]
