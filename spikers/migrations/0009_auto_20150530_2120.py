# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('spikers', '0008_auto_20150530_2104'),
    ]

    operations = [
        migrations.AlterField(
            model_name='spikers',
            name='userphoto',
            field=models.ImageField(default=0, upload_to=b'users/photo/', blank=True),
            preserve_default=False,
        ),
    ]
