#! coding: utf-8
from django.shortcuts import render

# функция генерирующая 404 страницу
from django.http import Http404

# Create your views here.
from .models import Spikers

def index(request):
	spikers_list = Spikers.objects.all()
	context = {'spikers_list': spikers_list}
	return render(request, 'spikers/index.html', context)
	
def detail(request, user_id):
	try:
		spiker = Spikers.objects.get(id=user_id)
	except Spikers.DoesNotExist:
		#если такого поста нет, то генерируем 404
		#raise Http404
		#включенный DEBUG=True блокирует отображение
		#стандартных шаблонов, по этому использую прямое включение
		return render(request, '404.html')
	return render(request, 'spikers/detail.html', {'spiker': spiker})