#! coding: utf-8
from django.shortcuts import render
from django.http import HttpResponse

#элементы для пагинатора
#http://djbook.ru/rel1.4/topics/pagination.html
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

#https://docs.djangoproject.com/en/dev/ref/models/querysets/
#News.objects.order_by(Lower('pub_date').desc())
#.order_by(Lower('pub_date').desc()) необходим для обратной сортировки выводимых новостей по дате добавления
from django.db.models.functions import Lower 

# функция генерирующая 404 страницу
from django.http import Http404

# подключение библиотеки для работы с json
# https://rayed.com/wordpress/?p=1508
from django.core import serializers

# Create your views here.
from .models import News

def index(request):
	news_list = News.objects.order_by(Lower('pub_date').desc())

	paginator = Paginator(news_list, 3) #Показать 3 новостей на странице
	page = request.GET.get('page')
	try:
		news = paginator.page(page)
	except PageNotAnInteger:
        # Если "page" не является "integer", показать первую страницу
		news = paginator.page(1)
	except EmptyPage:
        # Если страница находится вне диапазона (например 9999), поставить последнюю страницу результатов.
		news = paginator.page(paginator.num_pages)
	context = {'news_list': news}
	return render(request, 'news/index.html', context)
	
def detail(request, news_id):
	try:
		news_data = News.objects.get(id=news_id)
	except News.DoesNotExist:
		#если такого поста нет, то генерируем 404
		#raise Http404
		#включенный DEBUG=True блокирует отображение
		#стандартных шаблонов, по этому использую прямое включение
		return render(request, '404.html')
	return render(request, 'news/detail.html', {'news': news_data})
	
def json(request):
	news_list = News.objects.order_by(Lower('pub_date').desc())
	#return HttpResponse(JsonResponse(news_list, safe=True), content_type='application/json')
	
	#tasks = Task.objects.all()
	data = serializers.serialize("json", news_list)
	return HttpResponse(data, content_type='application/json')