#! coding: utf-8
from django.db import models
class News(models.Model):
	title = models.CharField(max_length=70)
	description = models.TextField()
	pub_date = models.DateTimeField()
	text = models.TextField()
	def __str__(self):
		return self.title