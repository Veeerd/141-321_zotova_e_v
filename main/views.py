#! coding: utf-8
from django.shortcuts import render

from django.conf import settings
from datetime import datetime
from datetime import date
from django.utils import dateformat
now = datetime.now()

# Create your views here.
# Отображает только те конференции, которые проходят сегодня (дата проведения конференции==текущая дата)
from presentation.models import Presentation
def index(request):
	presentation_list = Presentation.objects.filter(pub_date__gt=date(now.year,now.month,now.day))
	context = {'presentation_list': presentation_list, 'date_now': date(now.year,now.month,now.day)}
	return render(request, 'main/index.html', context)

