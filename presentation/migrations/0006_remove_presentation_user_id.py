# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('presentation', '0005_auto_20150529_1733'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='presentation',
            name='user_id',
        ),
    ]
