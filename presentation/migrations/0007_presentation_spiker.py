# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('spikers', '__first__'),
        ('presentation', '0006_remove_presentation_user_id'),
    ]

    operations = [
        migrations.AddField(
            model_name='presentation',
            name='spiker',
            field=models.ForeignKey(default=0, to='spikers.Spikers'),
            preserve_default=False,
        ),
    ]
