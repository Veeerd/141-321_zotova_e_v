# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('presentation', '0008_auto_20150529_2016'),
    ]

    operations = [
        migrations.AlterField(
            model_name='presentation',
            name='description',
            field=models.TextField(),
        ),
    ]
