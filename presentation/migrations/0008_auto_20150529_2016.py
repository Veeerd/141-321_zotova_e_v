# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('presentation', '0007_presentation_spiker'),
    ]

    operations = [
        migrations.AlterField(
            model_name='presentation',
            name='pub_date',
            field=models.DateTimeField(),
        ),
    ]
