# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('presentation', '0002_auto_20150525_2055'),
    ]

    operations = [
        migrations.AddField(
            model_name='presentation',
            name='user_id',
            field=models.IntegerField(default=0),
            preserve_default=False,
        ),
    ]
