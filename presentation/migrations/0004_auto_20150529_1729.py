# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('presentation', '0003_presentation_user_id'),
    ]

    operations = [
        migrations.AlterField(
            model_name='presentation',
            name='user_id',
            field=models.ForeignKey(to='spikers.Spikers'),
        ),
    ]
