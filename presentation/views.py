#! coding: utf-8
from django.shortcuts import render

# функция генерирующая 404 страницу
from django.http import Http404

# Create your views here.
from .models import Presentation

def index(request):
	presentation_list = Presentation.objects.all()
	context = {'presentation_list': presentation_list}
	return render(request, 'presentation/index.html', context)
	
def detail(request, presentation_id):
	try:
		presentation= Presentation.objects.get(id=presentation_id)
	except Presentation.DoesNotExist:
		#если такого поста нет, то генерируем 404
		#raise Http404
		#включенный DEBUG=True блокирует отображение
		#стандартных шаблонов, по этому использую прямое включение
		return render(request, '404.html')
	return render(request, 'presentation/detail.html', {'presentation': presentation})