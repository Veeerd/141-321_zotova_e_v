from django.db import models

# Create your models here.
from spikers.models import Spikers

class Presentation(models.Model):
	spiker = models.ForeignKey(Spikers)
	title = models.CharField(max_length=70)
	description = models.TextField()
	pub_date = models.DateTimeField()
	def __str__(self):
		return self.title
