#! coding: utf-8
from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse

#Кэширование на уровне представлений http://djbook.ru/rel1.5/topics/cache.html#the-per-view-cache
from django.views.decorators.cache import cache_page

# функция генерирующая 404 страницу
from django.http import Http404

# Create your views here.
from .forms import PartnersForm
from .forms import PartnersForm_Edit
from .models import Partners

def index(request):
	partners_list = Partners.objects.all()
	context = {'partners_list': partners_list}
	return render(request, 'partners/index.html', context)

@cache_page(60 * 15)
def add(request):
	if request.method == 'POST':
		form = PartnersForm(request.POST, request.FILES)
		if form.is_valid():
			form.save()
			return HttpResponseRedirect('/partners/')
	else:
		form = PartnersForm()
	
	return render(request, 'partners/add.html', {'form': form})

def delete(request, partner_id):
	try:
		Partners.objects.get(id=partner_id).delete()
	except Partners.DoesNotExist:
		return HttpResponseRedirect('/partners/')
	return HttpResponseRedirect('/partners/')

@cache_page(60 * 3)
def edit(request, partner_id):
	if request.method == 'POST':
		form = PartnersForm_Edit(request.POST)
		if form.is_valid():
			data_dict = {'partner_name': request.POST["partner_name"],'description': request.POST["description"]}
			Partners.objects.filter(id=partner_id).update(**data_dict)
			return HttpResponseRedirect('/partners/')
	else:
		try:
			article = Partners.objects.get(id=partner_id)
			form = PartnersForm_Edit(instance=article)
		except Partners.DoesNotExist:
			#если такого поста нет, то генерируем 404
			#raise Http404
			#включенный DEBUG=True блокирует отображение
			#стандартных шаблонов, по этому использую прямое включение
			return render(request, '404.html')
	return render(request, 'partners/edit.html', {'form': form,'partner_id':partner_id})
