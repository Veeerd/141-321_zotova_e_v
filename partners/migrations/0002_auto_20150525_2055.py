# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('partners', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='partners',
            name='pub_date',
        ),
        migrations.RemoveField(
            model_name='partners',
            name='text',
        ),
        migrations.RemoveField(
            model_name='partners',
            name='title',
        ),
        migrations.AddField(
            model_name='partners',
            name='img',
            field=models.ImageField(default=0, upload_to=b''),
            preserve_default=False,
        ),
    ]
