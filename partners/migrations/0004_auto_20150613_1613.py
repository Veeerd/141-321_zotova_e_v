# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('partners', '0003_auto_20150530_1844'),
    ]

    operations = [
        migrations.AlterField(
            model_name='partners',
            name='description',
            field=models.TextField(),
        ),
    ]
