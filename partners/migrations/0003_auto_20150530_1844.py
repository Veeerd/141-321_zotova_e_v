# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('partners', '0002_auto_20150525_2055'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='partners',
            name='img',
        ),
        migrations.AddField(
            model_name='partners',
            name='logotype',
            field=models.ImageField(default=0, upload_to=b'partners/logotype'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='partners',
            name='partner_name',
            field=models.CharField(default=0, max_length=255),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='partners',
            name='description',
            field=models.CharField(max_length=1000),
        ),
    ]
