from django.db import models

# Create your models here.
class Partners(models.Model):
	logotype = models.ImageField(upload_to='partners/logotype')
	partner_name =  models.CharField(max_length=255)
	description = models.TextField()
  	def __str__(self):
		return self.partner_name
		