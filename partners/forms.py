#! coding: utf-8
from django import forms
from .models import Partners

class PartnersForm(forms.ModelForm):
   class Meta:
		model = Partners
		fields = ['logotype','partner_name','description']
		labels = {
            'logotype': ('Логотип'),
			'partner_name': ('Наименование'),
			'description': ('Описание'),
        }

class PartnersForm_Edit(forms.ModelForm):
   class Meta:
		model = Partners
		fields = ['partner_name','description']
		labels = {
			'partner_name': ('Наименование'),
			'description': ('Описание'),
        }
