from django.conf.urls import patterns, url
from partners import views
urlpatterns = patterns('',
	url(r'^$', views.index, name='index'),
    url(r'^add/', views.add, name='add'),
	url(r'^(?P<partner_id>[0-9]+)/delete/$', views.delete, name='delete'),
	url(r'^(?P<partner_id>[0-9]+)/edit/$', views.edit, name='edit'), 
)
